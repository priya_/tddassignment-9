package com.PowerOfFour;

public class ValidatePowerOfFour {
    private static read readnumber = new read();
    private static int n;

    public void set(read file) throws Exception {
        this.readnumber = file;
    }


    public static boolean isPowerOfFour() throws Exception{
        n = readnumber.reader("input.txt");
        System.out.println("value from text file=" + n);
        return n != 0 && ((n & (n - 1)) == 0) && (n & 0xAAAAAAAA) == 0;//logic for whether a number is power of 4
    }
}
