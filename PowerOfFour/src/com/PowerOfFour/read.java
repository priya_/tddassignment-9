package com.PowerOfFour;
import java.nio.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class read {
    public int reader(String s) throws Exception
    {
        Path file= Paths.get(s);
        Scanner scanner= new Scanner(file);
        int num=scanner.nextInt();

        return num;
    }
}
